//
//  SecondTabInteractor.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

class SecondTabInteractor: SecondTabInteractorProtocol {
    weak var presenter: SecondTabPresenterProtocol?
    var dataManager: SecondTabDataManagerProtocol?
    
    func retrievePostList() {
        if let postList = dataManager?.retrievePostList() {
            presenter?.didRetrievePostList(posts: postList)
        } else {
            presenter?.didRetrievePostList(posts: [])
        }
    }
}
