//
//  SecondTabWireFrame.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import UIKit

class SecondTabWireFrame: SecondTabWireFrameProtocol {
    weak var presenter: SecondTabPresenterProtocol?
    
    static var mainStoryboard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    static func createSecondTabModule() -> UIViewController {
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "SecondToDetailNavController")
        if let view = navController.childViewControllers.first as? SecondTabView {
            let presenter: SecondTabPresenterProtocol = SecondTabPresenter()
            let interactor: SecondTabInteractorProtocol = SecondTabInteractor()
            let datamanager: SecondTabDataManagerProtocol = SecondTabDataManager()
            let wireFrame: SecondTabWireFrameProtocol = SecondTabWireFrame()
            
            view.presenter = presenter
            presenter.view = view
            presenter.wireFrame = wireFrame
            interactor.presenter = presenter
            presenter.interactor = interactor
            datamanager.interactor = interactor
            interactor.dataManager = datamanager
            
            return navController
        }
        return UIViewController()
    }
    
    func presentPostDetailScreen(from view: SecondTabViewProtocol, forPost post: PostModel) {
        let postDetailViewController = PostDetailWireFrame.createPostDetailModule(forPost: post)
        
        if let sourceView = view as? UIViewController {
            sourceView.navigationController?.pushViewController(postDetailViewController, animated: true)
        }
    }
}
