//
//  SecondTabPresenter.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

class SecondTabPresenter: SecondTabPresenterProtocol{
    weak var view: SecondTabViewProtocol?
    var interactor: SecondTabInteractorProtocol?
    var wireFrame: SecondTabWireFrameProtocol?
    
    func viewDidLoad() {
        interactor?.retrievePostList()
    }
    
    func showPostDetail(forPost post: PostModel) {
        wireFrame?.presentPostDetailScreen(from: view!, forPost: post)
    }
    
    func didRetrievePostList(posts: [PostModel]) {
        view?.showPost(with: posts)
    }
}
