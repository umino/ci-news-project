//
//  SecondTabProtocol.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import UIKit

protocol SecondTabViewProtocol: class {
    var presenter: SecondTabPresenterProtocol? { get set }
    
    func showPost(with post: [PostModel])
}

protocol SecondTabPresenterProtocol: class {
    var view: SecondTabViewProtocol? { get set }
    var interactor: SecondTabInteractorProtocol? { get set }
    var wireFrame: SecondTabWireFrameProtocol? { get set }
    
    func viewDidLoad()
    func didRetrievePostList(posts: [PostModel])
    func showPostDetail(forPost post: PostModel)
}

protocol SecondTabWireFrameProtocol: class {
    //var presenter: SecondTabPresenterProtocol? { get set }
    
    static func createSecondTabModule() -> UIViewController
    func presentPostDetailScreen(from view: SecondTabViewProtocol, forPost post: PostModel)
}

protocol SecondTabInteractorProtocol: class {
    var presenter: SecondTabPresenterProtocol? { get set }
    var dataManager: SecondTabDataManagerProtocol? { get set }
    
    // input
    func retrievePostList()
}

protocol SecondTabDataManagerProtocol: class {
    var interactor: SecondTabInteractorProtocol? { get set }
    // get post list from entities
    func retrievePostList() -> [PostModel]
}
