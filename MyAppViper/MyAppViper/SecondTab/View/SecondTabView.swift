//
//  SecondTabView.swift
//  MyAppViper
//
//  Created by thi la on 3/6/18.
//

import UIKit

class SecondTabView: UIViewController {
    @IBOutlet weak var collectionView: UICollectionView?
    var postList:[PostModel] = []
    var presenter: SecondTabPresenterProtocol?
    let identifier = "Cell"
    lazy var customCellPrototype = UINib(nibName: identifier, bundle: nil).instantiate(withOwner: nil, options: nil).first as! SecondTabCollectionCell
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customCellPrototype.translatesAutoresizingMaskIntoConstraints = false
        presenter?.viewDidLoad()
    }
}

extension SecondTabView: SecondTabViewProtocol {
    func showPost(with post: [PostModel]) {
        postList = post
        
        collectionView?.register(UINib.init(nibName: identifier, bundle: nil), forCellWithReuseIdentifier: identifier)
        collectionView?.reloadData()
    }
}

extension SecondTabView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath) as? SecondTabCollectionCell else {
            fatalError("The dequeued cell is not an instance of SecondTabCollectionCell.")
        }
        let post = postList[indexPath.row]
        cell.set(forPost: post)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
       return postList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.showPostDetail(forPost:postList[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // create a cell template for getting cell's sizes
        customCellPrototype.set(forPost: postList[indexPath.row])
        customCellPrototype.layoutSubviews()
        let width = UIScreen.main.bounds.width - 16*2
        
        var fittedSize = UILayoutFittingCompressedSize
        fittedSize.width = width
        let nSize = customCellPrototype.contentView.systemLayoutSizeFitting(fittedSize, withHorizontalFittingPriority: UILayoutPriority.required, verticalFittingPriority: UILayoutPriority.fittingSizeLevel)
        
        return nSize
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { (_) in
            self.collectionView?.reloadData()
        }, completion: nil)
    }
}
