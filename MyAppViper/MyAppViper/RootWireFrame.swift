//
//  RootWireFrame.swift
//  MyAppViper
//
//  Created by thi la on 3/7/18.
//

import UIKit

class RootWireFrame {
    static var mainStoryBoard: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    static func createRootModule() -> UIViewController {
        if let tabBarController = mainStoryBoard.instantiateViewController(withIdentifier: "TabBarController") as? UITabBarController {
            
            let firstTabController = FirstTabWireFrame.createFirstTabModule()
            let secondTabController = SecondTabWireFrame.createSecondTabModule()
            let listController = [firstTabController, secondTabController]
            tabBarController.viewControllers = listController
            
            return tabBarController
        }
        return UIViewController()
    }
}

