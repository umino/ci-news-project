//
//  PostDetailView.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

import UIKit
import SDWebImage

class PostDetailView: UIViewController {
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var ivThumbnail: UIImageView!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var lbDate: UILabel!
    @IBOutlet weak var lbViewNumber: UILabel!
    
    var presenter: PostDetailPresenterProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        presenter?.viewDidLoad()
    }
}

extension PostDetailView: PostDetailViewProtocol {
    func showPostDetail(forPost post: PostModel) {
        lbTitle?.text = post.title
        lbContent?.text = post.content
        lbDate?.text = post.createAt
        lbViewNumber?.text = post.viewedNumber
        
        if post.imageUrl != nil {
            ivThumbnail.sd_setImage(with: URL(string: post.imageUrl!))
        }
    }
}
