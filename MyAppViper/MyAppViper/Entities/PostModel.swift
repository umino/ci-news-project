//
//  PostModel.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

import Foundation

struct PostModel {
    var id: Int32 = 0
    var title: String?
    var imageUrl: String?
    var content: String?
    var createAt: String?
    var viewedNumber: String?
    
    init (dictionary: [String: AnyObject]) {
        guard let id = dictionary["id"] as? Int32,
            let title = dictionary["title"] as? String else {
            // post can't miss id and title
            return
        }
        self.id = id
        self.title = title
        self.content = dictionary["content"] as? String
        self.imageUrl = dictionary["imageUrl"] as? String
        self.createAt = dictionary["createAt"] as? String
        self.viewedNumber = dictionary["viewedNumber"] as? String
    }
}
