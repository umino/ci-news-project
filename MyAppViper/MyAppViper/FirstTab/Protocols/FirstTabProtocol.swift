//
//  FirstTabProtocol.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//
import UIKit

protocol FirstTabProtocol: class {
    var presenter: FirstTabPresenterProtocol? { get set }
    
    // Presenter -> View
    func showPosts(with posts: [PostModel])
}

protocol FirstTabWireFrameProtocol: class {
    static func createFirstTabModule() -> UIViewController
    // Presenter -> Wireframe
    func presentPostDetailScreen(from view: FirstTabProtocol, forPost post: PostModel)
}

protocol FirstTabPresenterProtocol: class {
    var view: FirstTabProtocol? { get set }
    var interactor: FirstTabInteractorProtocol? { get set }
    var wireFrame: FirstTabWireFrameProtocol? { get set }
    
    // View -> Presenter
    func viewDidLoad()
    func didRetrievePostList(posts: [PostModel])
    func showPostDetail(forPost post: PostModel)
}

protocol FirstTabInteractorProtocol: class {
    var presenter: FirstTabPresenterProtocol? { get set }
    var dataManager: FirstTabDataManagerProtocol? { get set }
    
    // Interactor -> Presenter
    func retrievePostList()
}

protocol FirstTabDataManagerProtocol: class{
    var interactor: FirstTabInteractorProtocol? { get set}
    // Interactor -> DataManager
    // get post list from entities
    func retrievePostList() -> [PostModel]
}










