//
//  FirstTabPresenter.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

class FirstTabPresenter: FirstTabPresenterProtocol {
    weak var view: FirstTabProtocol?
    var interactor: FirstTabInteractorProtocol?
    var wireFrame: FirstTabWireFrameProtocol?
    
    func viewDidLoad() {
        interactor?.retrievePostList()
    }
    
    func showPostDetail(forPost post: PostModel) {
        wireFrame?.presentPostDetailScreen(from: view!, forPost: post)
    }
    
    func didRetrievePostList(posts: [PostModel]) {
        view?.showPosts(with: posts)
    }
}
