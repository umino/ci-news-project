//
//  FirstTabInteractor.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

class FirstTabInteractor: FirstTabInteractorProtocol {
    weak var presenter: FirstTabPresenterProtocol?
    var dataManager: FirstTabDataManagerProtocol?
    
    func retrievePostList() {
        if let postList = dataManager?.retrievePostList() {
            presenter?.didRetrievePostList(posts: postList)
        } else {
            presenter?.didRetrievePostList(posts: [])
        }
    }
}
