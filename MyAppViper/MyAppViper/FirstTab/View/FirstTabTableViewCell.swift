//
//  FirstTabTableViewCell.swift
//  MyAppViper
//
//  Created by thi la on 3/5/18.
//

import UIKit
import SDWebImage

class FirstTabTableViewCell: UITableViewCell {
    
    @IBOutlet weak var lbTitle: UILabel!
    @IBOutlet weak var lbContent: UILabel!
    @IBOutlet weak var ivThumbnail: UIImageView!
    
    func set(forPost post: PostModel) {
        //self.selectionStyle = .none
        lbTitle?.text = post.title
        lbContent?.text = post.content?.components(separatedBy: ".")[0]
        if post.imageUrl != nil {
            ivThumbnail.sd_setImage(with: URL(string: post.imageUrl!))
        }
    }
}
