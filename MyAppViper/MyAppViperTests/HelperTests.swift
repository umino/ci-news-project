//
//  HelperTests.swift
//  MyAppViperTests
//
//  Created by thi la on 3/19/18.
//

import XCTest
@testable import MyAppViper

class HelperTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testGetPostListFromLocalFile() {
        let postList = Helper.getPostListFromLocalFile()
        
        XCTAssertNotNil(postList, "Could not load data, post list is nil")
        XCTAssert(postList?.count == 7, "Load data failed, post list count is not exact")
        let sampleTitle = "Bức ảnh gây tranh cãi của Tổng thống Putin"
        XCTAssertEqual(postList?[0].title, sampleTitle, "post list content is not exact")
    }
}
