//
//  SecondTabWireFrameTests.swift
//  MyAppViperTests
//
//  Created by thi la on 3/19/18.
//

import XCTest
@testable import MyAppViper

class SecondTabWireFrameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitializeSecondTabView() {
        let mainStoryboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let navController = mainStoryboard.instantiateViewController(withIdentifier: "SecondToDetailNavController")
        let firstTabView = navController.childViewControllers.first as? SecondTabView
        
        XCTAssertNotNil(firstTabView, "Cannot create SecondTabView")
    }
    
}
